<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Interface\PetInterface;
use Exception;


class Farm
{
    private const DAYS = 7;
    private array $registryPet;
    private PetInterface $pet;

    /**
     * @throws Exception
     */
    public function buyPet(string $buyPet): self
    {
        if(class_exists($buyPet)){
            $createPet = new $buyPet;
            /** @var AbstractFactoryInterface $pet */
            if($createPet instanceof AbstractFactoryInterface){
                $pet = $createPet->createPet()->setProduct($createPet->createProduct());
                $this->pet = $pet;
                $this->addToRegistry();
            }
        } else {
            throw new Exception('Class: ' . $buyPet . ' not exist');
        }
            return $this;
    }

    /**
     * @return Farm
     */
    public function addToRegistry(): self
    {
        $this->registryPet[] = $this->pet;
        return $this;
    }

    public function serviceCollectProduct(): void
    {
        $total = 0;
            foreach ($this->getRegistryPet() as $item) {

                for ($i = 0; $i < self::DAYS; $i++) {
                    if (!$item instanceof PetInterface) {
                        continue;
                    }
                    $oneDay = $item->collectProductPerOneDay();
                    $total += $oneDay;
                }

                $item->getProduct()->setQuantity($total);
            }

    }

    public function getPetStatistic(): array
    {
        $pets = [];
        $total = [];

        foreach ($this->getRegistryPet() as $item){
            if(!$item instanceof PetInterface){
                continue;
            }

            $pets[$item->getType()][$item->getNumber()] = $item;
            $count = count($pets[$item->getType()]);
            $total[$item->getType()] = $count;
        }

        return $total;
    }

    public function getProductStatistic(): array
    {
        $products = [];
        $total = [];
        $count = 0;
        foreach ($this->getRegistryPet() as $item){
            if(!$item instanceof PetInterface){
                continue;
            }

            $products[$item->getProduct()->getName()] = $item->getProduct()->getQuantity();
            $count += $products[$item->getProduct()->getName()];
            $total[$item->getProduct()->getName()] = $count . ' ' . $item->getProduct()::UNIT;
        }

        return $total;
    }

    /**
     * @return array
     */
    public function getRegistryPet(): array
    {
        return $this->registryPet;
    }

    /**
     * @return PetInterface
     */
    public function getPet(): PetInterface
    {
        return $this->pet;
    }

}