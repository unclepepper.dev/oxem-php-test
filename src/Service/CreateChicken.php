<?php

declare(strict_types=1);

namespace App\Service;


use App\Entity\Chicken;
use App\Entity\Egg;
use App\Entity\Interface\PetInterface;
use App\Entity\Interface\ProductInterface;
use Exception;


class CreateChicken implements AbstractFactoryInterface
{

    /**
     * @throws Exception
     */
    public  function createPet(): PetInterface
    {
        if(class_exists(Chicken::class))
            return new Chicken();
        else
            throw new Exception('Class: ' . Chicken::class . ' not exist');
    }

    /**
     * @throws Exception
     */
    public  function createProduct(): ProductInterface
    {
        if(class_exists(Egg::class))
            return new Egg();
        else
            throw new Exception('Class: ' . Egg::class . ' not exist');
    }
}