<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Cow;
use App\Entity\Interface\PetInterface;
use App\Entity\Interface\ProductInterface;
use App\Entity\Milk;
use Exception;


class CreateCow implements AbstractFactoryInterface
{

    /**
     * @throws Exception
     */
    public  function createPet(): PetInterface
    {
        if(class_exists(Cow::class))
            return new Cow();
        else
            throw new Exception('Class: ' . Cow::class . ' not exist');
    }

    /**
     * @throws Exception
     */
    public  function createProduct(): ProductInterface
    {
        if(class_exists(Milk::class))
            return new Milk();
        else
            throw new Exception('Class: ' . Milk::class . ' not exist');
    }
}