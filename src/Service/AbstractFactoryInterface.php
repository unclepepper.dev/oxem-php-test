<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Interface\PetInterface;
use App\Entity\Interface\ProductInterface;


interface AbstractFactoryInterface
{

    public function createPet(): PetInterface;
    public function createProduct(): ProductInterface;

}