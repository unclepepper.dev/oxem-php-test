<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Interface\PetInterface;
use App\Entity\Interface\ProductInterface;
use Exception;

class Cow implements PetInterface
{
    private string $number;
    private string $type;
    private string $name;
    private ProductInterface $product;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->number = uniqid();
        $this->type = basename(str_replace('\\', '/', __CLASS__));
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }


    public function collectProductPerOneDay():int
    {
        return rand(8, 12);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Cow
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    /**
     * @param ProductInterface $product
     * @return Cow
     */
    public function setProduct(ProductInterface $product): self
    {
        $this->product = $product;
        return $this;
    }

}