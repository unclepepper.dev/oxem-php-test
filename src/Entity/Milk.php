<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Interface\ProductInterface;

class Milk implements ProductInterface
{
    public const UNIT = 'liters';
    private string $name;
    private int $quantity;

    public function __construct()
    {
        $this->name = basename(str_replace('\\', '/', __CLASS__));
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Milk
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

}