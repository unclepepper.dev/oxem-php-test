<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Interface\ProductInterface;

class Egg implements ProductInterface
{
    public const UNIT = 'pieces';
    private string $name;
    private int $quantity;

    public function __construct()
    {
        $this->name = basename(str_replace('\\', '/', __CLASS__));
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Egg
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

}