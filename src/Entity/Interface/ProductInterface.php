<?php

declare(strict_types=1);

namespace App\Entity\Interface;

interface ProductInterface
{
    public function getName(): string;
    public function getQuantity(): int;
    public function setQuantity(int $quantity): self;
}