<?php

declare(strict_types=1);

namespace App\Entity\Interface;

interface PetInterface
{
    public function getNumber(): string;
    public function getType(): string;
    public function getName(): string;
    public function setName(string $name): self;
    public function collectProductPerOneDay():int;
    public function getProduct(): ProductInterface;
    public function setProduct(ProductInterface $product): self;
}