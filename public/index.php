<?php

declare(strict_types=1);

use App\Service\CreateChicken;
use App\Service\CreateCow;
use App\Service\Farm;



require_once dirname(__DIR__).'/vendor/autoload.php';


$farm = new Farm();

// added Cow - 10 and Chicken - 20:

for($i = 0; $i < 10; $i++) $farm->buyPet(CreateCow::class);
for($i = 0; $i < 20; $i++) $farm->buyPet(CreateChicken::class);

// output result:
echo 'Added: ' . PHP_EOL;
foreach ($farm->getPetStatistic() as $key => $item) echo $key . ' - ' . $item . '; ' . PHP_EOL;

// collection product:
$farm->serviceCollectProduct();

// output result:
for($i = 0; $i < 10; $i++) print ' . ' . usleep(90000);

echo  PHP_EOL . 'Collected: ' . PHP_EOL;
foreach ($farm->getProductStatistic() as $key => $item) echo $key . ' - ' . $item . '; ' . PHP_EOL;

sleep(1);
print  PHP_EOL . 'Went to the market and bought' . PHP_EOL;


// added Cow - 1 and Chicken - 5:
for($i = 0; $i < 1; $i++) $farm->buyPet(CreateCow::class);
for($i = 0; $i < 5; $i++) $farm->buyPet(CreateChicken::class);

// output result:
echo  PHP_EOL . 'Total: ' . PHP_EOL;
foreach ($farm->getPetStatistic() as $key => $item) echo $key . ' - ' . $item . '; ' . PHP_EOL;

// collection product:
$farm->serviceCollectProduct();

// output result:
for($i = 0; $i < 10; $i++) print ' . ' . usleep(90000);

echo  PHP_EOL . 'Collected: ' . PHP_EOL;
foreach ($farm->getProductStatistic() as $key => $item) echo $key . ' - ' . $item . '; ' . PHP_EOL;






